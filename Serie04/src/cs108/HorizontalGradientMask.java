package cs108;

public class HorizontalGradientMask implements Image<Double> {

    public static final Image<Double> IMAGE = new HorizontalGradientMask();

    @Override
    public Double apply(double x, double y) {
        if (x < -1) return 0.;
        if (x > 1) return 1.;
        return (x + 1) / 2;
    }
}
