package cs108;

/**
 * Échiquier à cellules carrées noires et blanches.
 */
public class Chessborad implements Image {

    private ColorRGB color1;
    private ColorRGB color2;
    private double size;

    public final static Image IMAGE = new Chessborad(ColorRGB.RED, ColorRGB.GREEN, 0.05);

    public Chessborad(ColorRGB color1, ColorRGB color2, double size) {
        if (size < 0) throw new IllegalArgumentException();
        this.color1 = color1;
        this.color2 = color2;
        this.size = size;
    }

    @Override
    public ColorRGB apply(double x, double y) {
        return (Math.floor(y/size) + Math.floor(x/size))%2 == 0 ? color1 : color2;
    }
}
