package cs108;

import java.awt.*;

public final class Composed implements Image<ColorRGB> {

    private final Image<Double> mask;
    private final Image<ColorRGB> background;
    private final Image<ColorRGB> foreground;

    public Composed(Image<Double> mask,
                    Image<ColorRGB> background, Image<ColorRGB> foreground) {
        this.mask = mask;
        this.background = background;
        this.foreground = foreground;
    }

    @Override
    public ColorRGB apply(double x, double y) {
        return background.apply(x,y).mixWith(foreground.apply(x,y), mask.apply(x,y));
    }
}
