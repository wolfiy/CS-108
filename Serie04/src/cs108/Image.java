package cs108;

/**
 * Représentation générique d'une image.
 */
public interface Image<T> {
    T apply(double x, double y);
}
