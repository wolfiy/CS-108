package cs108;

/**
 * Une image continue et infinie, représentée par une fonction associant une
 * couleur à chaque point du plan.
 */

/**
 * Concept d'image continue.
 */
public interface ImageRGB {

    /*
     * Associe une couleur à un pixel.
     */
    ColorRGB apply(double x, double y);
}
