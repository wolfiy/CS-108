package cs108;

import static java.lang.Math.sqrt;

/**
 * Disk rouge sur fond blanc.
 */
public final class RedDisk implements Image {
    public static final Image IMAGE = new RedDisk();
    
    @Override
    public ColorRGB apply(double x, double y) {
        double r = sqrt(x * x + y * y);
        return r <= 1d ? ColorRGB.RED : ColorRGB.WHITE;
    }
}