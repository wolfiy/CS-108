package cs108;

public final class Rotated<T> implements Image<T> {

    private final Image<T> image;
    private double angle;

    public Rotated(Image<T> image, double angle) {
        this.image = image;
        this.angle = angle;
    }

    @Override
    public T apply(double x, double y) {
        return image.apply(x * Math.cos(angle) + y * Math.sin(angle),
                           -x * Math.sin(angle) + y * Math.cos(angle));
    }
}
