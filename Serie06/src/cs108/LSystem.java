package cs108;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class LSystem {

    // Chaine de départ.
    private final String string;
    // Règles de réécriture.
    private final Map<Character, String> rules;
    // Ensemble des caractères provoquant le dessin d'une ligne.
    private final String lineChar;
    // Angle de rotation.
    private final int turningAngle;

    public LSystem(String string, Map<Character, String> rules, String lineChars, int turningAngle) {
        this.string = string;
        this.rules = Map.copyOf(rules);
        this.lineChar = lineChars;
        this.turningAngle = turningAngle;
    }

    public String string() {
        return string;
    }

    public Map<Character, String> rules() {
        return Map.copyOf(rules);
    }

    public Set<Character> lineChars() {
        Set <Character> lineCharsSet = new HashSet<>();

        for (int i = 0; i < lineChar.length(); ++i) {
            char usingThis = lineChar.charAt(i);
            if (usingThis != '+' && usingThis != '-') lineCharsSet.add(usingThis);
        }

        return lineCharsSet;
    }

    public int turningAngle() {
        return turningAngle;
    }

    public LSystem evolve() {
        String newString;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < string.length(); ++i) {
            char c = string.charAt(i);
            sb.append(rules.getOrDefault(c, String.valueOf(c)));
        }

        // Want: replace all F by F-F++F-F
        newString = sb.toString();
        return new LSystem(newString, rules, lineChar, turningAngle);
    }

    public LSystem evolve(int steps) {
        LSystem evolved = this;
        for (int i = 0; i < steps; ++i) evolved = evolved.evolve();
        return evolved;
    }
}
