package cs108;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public final class Steganographer {
    private Steganographer() { }

    public static String extract(BufferedImage image) {

        StringBuilder sb = new StringBuilder();
        int usedBits = 0;
        int bitCount = 0;
        int color;

        for (int i = 0; i < image.getHeight(); ++i) {
            for (int j = 0; j < image.getWidth(); ++j) {
                color = image.getRGB(j, i);
                usedBits = usedBits << 1 | (color & 1);
                if (++bitCount == Character.SIZE) {
                    sb.append((char) usedBits);
                    usedBits = 0;
                    bitCount = 0;
                }
            }
        }

        return sb.toString();
    }

    public static BufferedImage insert(BufferedImage image, String string) {
        BufferedImage outImage =
                new BufferedImage(image.getWidth(),
                        image.getHeight(),
                        BufferedImage.TYPE_INT_RGB);
        char chr = 0;
        int chrI = 0, bitI = 0;
        for (int y = 0; y < image.getHeight(); ++y) {
            for (int x = 0; x < image.getWidth(); ++x) {
                if (bitI == 0) {
                    chr = chrI < string.length()
                            ? string.charAt(chrI++)
                            : 0;
                    bitI = Character.SIZE;
                }
                int rgb = image.getRGB(x, y);
                int bit = (chr >> --bitI) & 1;
                outImage.setRGB(x, y, (rgb & ~1) | bit);
            }
        }
        return outImage;
    }
}
