package cs108;

import java.awt.*;
import java.util.stream.Stream;

import static java.lang.Math.*;

/**
 * Une image continue et infinie, représentée par une fonction associant une
 * valeur d'un type donné (p.ex. une couleur) à chaque point du plan.
 */

@FunctionalInterface
public interface Image<T> {
    T apply(double x, double y);

    public static final Image<ColorRGB> RED_DISK = (x,y) -> {
        double r = sqrt(x*x + y*y);
        return r <= 1d ? ColorRGB.RED : ColorRGB.WHITE;
    };

    public static Image<ColorRGB> chessboard(ColorRGB c1, ColorRGB c2, double w) {
        if (!(w>0)) throw new IllegalArgumentException("non-positive width: " + w);

        return (x,y) -> {
            int sqX = (int)floor(x / w), sqY = (int)floor(y / w);
            return (sqX + sqY) % 2 == 0 ? c1 : c2;
        };
    }

    public static Image<Double> mandelbrot(int maxIt) {
        if (! (maxIt > 0)) throw new IllegalArgumentException();

        return (x, y) -> {
            Complex c = new Complex(x, y);
            Complex z = c;
            int i = 1;
            while (i < maxIt && z.squaredModulus() <= 4d) {
                z = z.squared().add(c);
                i += 1;
            }
            return (double)i / (double)maxIt;
        };
    }

    // Corrigé:
    public static Image<Double> mandelbrot2(int maxIt) {
        if (! (maxIt > 0))
            throw new IllegalArgumentException();

        return (x, y) -> {
            Complex c = new Complex(x, y);
            double i = Stream.iterate(c, z -> z.squaredModulus() <= 4d, z -> z.squared().add(c))
                    .limit(maxIt)
                    .count();
            return i / maxIt;
        };
    }


    static <U> Image<U> rotated(Image<U> image, double angle) {
        double cosA = cos(-angle);
        double sinA = sin(-angle);

        return (x,y) -> {
            double x1 = x * cosA - y * sinA;
            double y1 = x * sinA + y * cosA;
            return image.apply(x1, y1);
        };
    }
}
