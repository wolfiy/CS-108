package cs108;

public record Complex(double re, double im) {
    public double squaredModulus() {
        return re * re + im * im;
    }

    public Complex squared() {
        return new Complex(re * re - im * im, 2d * re * im);
    }

    public Complex add(Complex that) {
        return new Complex(re + that.re, im + that.im);
    }
}
