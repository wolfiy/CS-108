package cs108;

/**
 * À pour but d'être implémentée par les classes désirant en observer d'autres et contient une
 * seule méthode qui est appelée lorsqu'un sujet observé par l'observateur a une nouvelle valeur.
 */
@FunctionalInterface
public interface Observer {
    void update(Subject s);
}
