package cs108;

import java.util.List;
import java.util.function.IntBinaryOperator;

/**
 * Cellule du tableur non observable, à modifier.
 */
public final class Cell extends AbstractSubject implements Observer {
    private final int column, row;

    private String contentString;
    private List<Cell> arguments;
    private IntBinaryOperator operator;
    private int value;

    public Cell(int column, int row, int initialValue) {
        this.column = column;
        this.row = row;
        this.contentString = String.valueOf(initialValue);
        this.arguments = List.of();
        this.operator = (x, y) -> initialValue;
        this.value = initialValue;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public String getName() {
        return String.format("%c%d", 'A' + column, row + 1);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int newValue) {
        if (value != newValue) {
            value = newValue;
            notifyObservers();
        }
    }

    public String getFormulaString() {
        return contentString;
    }

    public void setFormula(String newContentString,
                           List<Cell> newArguments,
                           IntBinaryOperator newOperator) {
        arguments.forEach(arg -> arg.removeSubject(this));
        newArguments.forEach(newArg -> newArg.addSubject(this));

        contentString = newContentString;
        arguments = newArguments;
        operator = newOperator;

        updateContent();
    }

    @Override
    public void update(Subject s) {
        updateContent();
    }

    private void updateContent() {
        int arg0 = arguments.size() > 0 ? arguments.get(0).getValue() : 0;
        int arg1 = arguments.size() > 1? arguments.get(1).getValue() : 0;

        setValue(operator.applyAsInt(arg0, arg1));
    }
}
