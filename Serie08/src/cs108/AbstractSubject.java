package cs108;

import java.util.ArrayList;
import java.util.List;

/**
 * Fournir des mises en oeuvre par défaut des méthodes d'ajout et de suppression d'observateurs,
 * ainsi qu'une méthode protégée permettant d'avertir tous les observateurs que le sujet a changé.
 * Cette dernière méthode a pour but d'être appelée par les sous-classes de AbstractSubj
 */
public class AbstractSubject implements Subject {

    private final List<Observer> observers = new ArrayList<>();

    @Override
    public void addSubject(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeSubject(Observer o) {
        observers.remove(o);
    }

    protected void notifyObservers() {
        observers.forEach(o -> o.update(this));
    }
}
