package cs108;

/**
 *  À pour but d'être implémentée par les classes pouvant être sujet d'observation et contient
 *  deux méthodes: l'une pour ajouter un observateur au sujet, l'autre pour le supprimer,
 */
public interface Subject {
    void addSubject(Observer o);

    void removeSubject(Observer o);
}
