package cs108;

import java.io.PrintStream;
import java.util.List;

public interface TextImage {
    int width();
    int height();
    List<String> drawing();

    static TextImage fromString(String s) {
        return new FromString(s);
    }

    static TextImage filled(int width, int height, char c) {
        return new Filled(width, height, c);
    }

    default TextImage transposed() {
        return new Transposed(this);
    }

    default TextImage flippedHorizontally() {
        return new FlippedHorizontally(this);
    }

    default TextImage leftOf(TextImage that) {
        return new LeftOf(this, that);
    }

    default TextImage above(TextImage that) {
        return new Above(this, that);
    }

    default TextImage framed() {
        TextImage a = fromString('+' + "-".repeat(width() + '+'));
        TextImage b = fromString("|".repeat(height())).transposed();
        return a.above(b.leftOf(this.leftOf(b)).above(a));
    }

    default void printOn(PrintStream stream) {
        for (String s : drawing()) stream.println(s);
    }
}