package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class LeftOf implements TextImage {
    private final TextImage left;
    private final TextImage right;

    public LeftOf(TextImage left, TextImage right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int width() {
        return left.width() + right.width();
    }

    @Override
    public int height() {
        return Math.max(left.height(), right.height());
    }

    @Override
    public List<String> drawing() {
        List<String> l = new ArrayList<>(width());
        // Needed the solution here.
        Iterator<String> li = left.drawing().iterator();
        Iterator<String> ri = right.drawing().iterator();
        while (li.hasNext() || ri.hasNext()) {
            String ll = li.hasNext() ? li.next() : " ".repeat(left.width());
            String rr = ri.hasNext() ? ri.next() : " ".repeat(right.width());
            l.add(ll + rr);
        }

        return Collections.unmodifiableList(l);
    }
}
