package cs108;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.nCopies;

final class Filled implements TextImage {
    private final int width;
    private final int height;
    private final String s;

    public Filled(int width, int height, char c) {
        if (width < 0 || height < 0) throw new IllegalArgumentException();
        this.width = width;
        this.height = height;
        s = String.valueOf(c).repeat(width);
    }

    @Override
    public int width() {
        return width;
    }

    @Override
    public int height() {
        return height;
    }

    @Override
    public List<String> drawing() {
        return nCopies(height, s);
    }
}
