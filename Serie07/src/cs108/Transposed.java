package cs108;

import java.util.ArrayList;
import java.util.List;

public class Transposed implements TextImage {
    private final TextImage img;

    public Transposed(TextImage img) {
        this.img = img;
    }

    @Override
    public int width() {
        return img.height();
    }

    @Override
    public int height() {
        return img.width();
    }

    @Override
    public List<String> drawing() {
        List<String> transposed = new ArrayList<>(height());
        List<String> toTranspose = img.drawing();

        for (int i = 0; i < height(); ++i) {
            StringBuilder sb = new StringBuilder();
            for (String s : toTranspose) sb.append(s.charAt(i));
            transposed.add(sb.toString());
        }

        return transposed;
    }
}
