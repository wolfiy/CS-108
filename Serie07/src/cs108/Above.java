package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Above implements TextImage {
    private final TextImage top;
    private final TextImage bottom;

    public Above(TextImage top, TextImage bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    @Override
    public int width() {
        return Math.max(bottom.width(), top.width());
    }

    @Override
    public int height() {
        return bottom.height() + top.height();
    }

    @Override
    public List<String> drawing() {
        List<String> l = new ArrayList<>(height());
        for (String s : top.drawing()) l.add(s + " ".repeat(width() - top.width()));
        for (String s : bottom.drawing()) l.add(s + " ".repeat(width() - bottom.width()));
        return Collections.unmodifiableList(l);
    }
}
