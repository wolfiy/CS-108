package cs108;

import java.util.List;

final class FromString implements TextImage {
    private final String s;
    public FromString(String s) {
        this.s = s;
    }

    @Override
    public int width() {
        return s.length();
    }

    @Override
    public int height() {
        return 1;
    }

    @Override
    public List<String> drawing() {
        return List.of(s);
    }
}
