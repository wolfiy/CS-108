package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FlippedHorizontally implements TextImage {
    private final TextImage img;

    public FlippedHorizontally(TextImage img) {
        this.img = img;
    }

    @Override
    public int width() {
        return img.width();
    }

    @Override
    public int height() {
        return img.height();
    }

    @Override
    public List<String> drawing() {
        List<String> normal = img.drawing();
        List<String> flipped = new ArrayList<>(normal.size());

        for (var v : normal) flipped.add(new StringBuilder(v).reverse().toString());

        return Collections.unmodifiableList(flipped);
    }
}
