package cs108;

public class Main {
    private final static int CELL_WIDTH = 3;
    private final static int CELL_HEIGHT = 2;

    public static void main(String[] args) {
        TextImage un = TextImage.fromString("La malade pédala mal");
        TextImage deux = TextImage.filled(3, 2, '*');
        un.printOn(System.out);
        deux.printOn(System.out);
        un.transposed().printOn(System.out);
        un.flippedHorizontally().printOn(System.out);
        un.above(un).printOn(System.out);
        un.leftOf(un).printOn(System.out);
        TextImage.filled(3, 1, 'X')
                 .above(TextImage.filled(4, 2, 'O'));
        TextImage.fromString("Un rectangle : ")
                 .leftOf(TextImage.filled(3, 2, '#'));
        TextImage black =
                TextImage.filled(CELL_WIDTH, CELL_HEIGHT, '#');
        TextImage white =
                TextImage.filled(CELL_WIDTH, CELL_HEIGHT, ' ');

        TextImage bw = black.leftOf(white);
        TextImage wb = bw.flippedHorizontally();
        TextImage board2 = bw.above(wb);
        TextImage board4 =
                (board2.leftOf(board2)).above(board2.leftOf(board2));
        TextImage board8 =
                (board4.leftOf(board4)).above(board4.leftOf(board4));
        board8.framed().printOn(System.out);
    }
}
