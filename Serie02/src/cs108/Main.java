package cs108;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;

public class Main {

    public static void main(String[] args) throws IOException {
        for (int f = 0; f <= 5; f += 1) {
            String fileName = "file%d.bin".formatted(f);
            System.out.printf("%n%s:%n", fileName);
            int[] freq = byteFrequencies(fileName);
            System.out.printf("moyenne: %.2f%n", average(freq));
            System.out.printf("entropie: %.2f bits/octet%n", entropy(freq));
            System.out.println("diagramme tige-feuille:");
            List<String> stemPlot = stemPlot(freq);
            for (String s : stemPlot)
                System.out.println(s);
        }
    }

    public static int[] byteFrequencies(String filename) throws IOException {
        try (InputStream stream = new FileInputStream(filename)) {
            int[] freq = new int[256];
            int a;
            while ((a = stream.read()) != -1) freq[a] += 1;
            return freq;
        }
    }

    private static double average(int[] freq) {
        int amount = 0;
        int length = 0;
        for (int i = 0; i < freq.length; ++i) {
            amount = amount + i * freq[i];
            length = length + freq[i];
        }
        return (double) amount / length;
    }

    private static double entropy(int[] freq) {
        int length = 0;
        for (int f : freq) length += f;
        double hNeg = 0;
        for (int f : freq) {
            if (f != 0) {
                double p = (double) f / (double) length;
                hNeg += p * log(p) / log(2);
            }
        }
        return -hNeg;
    }

    private static List<String> stemPlot(int[] freq) {
        // calcul du facteur de redimensionnement
        int maxLineLen = 0;
        for (int stem = 0; stem <= freq.length / 10; stem += 1) {
            int lineLen = 3; // 3 = longueur des tiges
            for (int leaf = 0; leaf <= 9; leaf += 1) {
                int i = 10 * stem + leaf;
                if (i >= freq.length) break;
                lineLen += freq[i];
            }
            maxLineLen = max(lineLen, maxLineLen);
        }
        double scale = min(1, 80d / maxLineLen);

        // calcul du diagramme tige et feuilles (redimensionné)
        List<String> lines = new ArrayList<>();
        for (int stem = 0; stem <= freq.length / 10; stem += 1) {
            StringBuilder lineBuilder =
                    new StringBuilder("%2d|".formatted(stem));
            for (int leaf = 0; leaf <= 9; leaf += 1) {
                int i = 10 * stem + leaf;
                if (i >= freq.length)
                    break;
                for (int k = 0; k < round(freq[i] * scale); k += 1)
                    lineBuilder.append(leaf);
            }
            lines.add(lineBuilder.toString());
        }
        return lines;
    }
}
