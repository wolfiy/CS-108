package cs108;

import java.util.*;

// 1. Toutes les rotations
// 2. Trier par ordre alpha
// 3. Dernière colonne
// 4. Index de la ligne avec str originale

public final class BurrowsWheelerTransform {
    private BurrowsWheelerTransform() {}

    public static Pair<Integer, String> forward(String s) throws IllegalArgumentException {
        if (s.isEmpty()) throw new IllegalArgumentException();

        // 1. Find all permutations
        LinkedList<Character> charQueue = new LinkedList<>();
        for (char ch : s.toCharArray()) charQueue.add(ch);

        List<String> rotationsList = allRotations(s);
        Collections.sort(rotationsList);

        StringBuilder sb = new StringBuilder(s.length());
        for (var v : rotationsList) sb.append(v.charAt(v.length() - 1));

        return new Pair<>(rotationsList.indexOf(s), sb.toString());
    }

    public static String backward(Pair<Integer, String> p) throws IndexOutOfBoundsException {
        int index = p.first();
        String s = p.second();

        if (! (0 <= index && index < s.length()))
            throw new IndexOutOfBoundsException();

        List<String> rs =
                new ArrayList<>(Collections.nCopies(s.length(), ""));
        for (int i = 0; i < s.length(); ++i) {
            for (int j = 0; j < s.length(); ++j)
                rs.set(j, s.charAt(j) + rs.get(j));
            Collections.sort(rs);
        }
        return rs.get(index);
    }

    public static List<String> allRotations(String s) {
        List<String> rotationsList = new ArrayList<>(s.length());

        // Want: shift all letters by one on the right at each incrementation.
        do {
            rotationsList.add(s);
            s = new StringBuilder(s.length()).append(s, 1, s.length())
                                              .append(s, 0, 1)
                                              .toString();
        } while (rotationsList.size() < s.length());

        return rotationsList;
    }
}
