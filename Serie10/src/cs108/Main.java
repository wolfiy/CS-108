package cs108;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public final class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Mandelbrot mandelbrot = new Mandelbrot();
        ImageView imageView = new ImageView();
        imageView.imageProperty().bind(mandelbrot.imageProperty());

        imageView.setOnMouseClicked(e -> {
            Rectangle frame = mandelbrot.getFrame();
            double imageToPlane = frame.width() / mandelbrot.width();
            double vRe = e.getX() * imageToPlane;
            double vIm = frame.height() - e.getY() * imageToPlane;

            if (e.getClickCount() == 1 && e.getButton() == MouseButton.SECONDARY) {
                // Recenter
                double cRe = frame.minX() + vRe;
                double cIm = frame.minY() + vIm;
                mandelbrot.setFrameCenter(new Point(cRe, cIm));
            } else if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
                // Zoom in (w/o control) or out (w/ control)
                double scale = (e.isControlDown() ? 2 : 0.5);
                Rectangle newFrame = frame
                        .translatedBy(vRe, vIm)
                        .scaledBy(scale)
                        .translatedBy(-vRe * scale, -vIm * scale);

                mandelbrot.setFrameCenter(newFrame.center());
                mandelbrot.setFrameWidth(newFrame.width());
            }
        });

        BorderPane mainPane = new BorderPane(imageView);

        mandelbrot.widthProperty().bind(mainPane.widthProperty());
        mandelbrot.heightProperty().bind(mainPane.heightProperty());

        primaryStage.setScene(new Scene(mainPane));
        primaryStage.setTitle("Mandelbrot");
        primaryStage.show();
    }
}
