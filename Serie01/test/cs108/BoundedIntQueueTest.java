package cs108;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

abstract class BoundedIntQueueTest {

    protected abstract BoundedIntQueue newBoundedIntQueue(int capacity);

    @Test
    void constructorWithNegativeCapacityFails() {
        assertThrows(IllegalArgumentException.class, () -> {
            newBoundedIntQueue(-1);
        });
    }

    @Test
    void capacityReturnsCorrectAmount() {
        for (int i = 0; i < 1000; ++i) {
            var expected = i;
            var actual = newBoundedIntQueue(i).capacity();
            assertEquals(expected, actual);
        }
    }

    @Test
    void initialSizeIsZero() {
        var expected = 0;
        var actual = newBoundedIntQueue(12345).size();
        assertEquals(expected, actual);
    }

    @Test
    void addingElementsToFullQueueFails() {
        BoundedIntQueue bq = newBoundedIntQueue(0);
        assertThrows(IllegalArgumentException.class, () -> {
            bq.addLast(1);
        });
    }

    @Test
    public void removingFromEmptyQueueFails() {
        BoundedIntQueue bq = newBoundedIntQueue(10);
        assertThrows(IllegalStateException.class, () -> {
            bq.removeFirst();
        });
    }

    @Test
    public void sizeReturnsCorrectValue() {
        int capacity = 100;
        BoundedIntQueue q = newBoundedIntQueue(capacity);
        for (int i = 0; i < capacity; ++i) {
            assertEquals(i, q.size());
            q.addLast(i);
        }
    }

    @Test
    public void isEmptyIsInitiallyTrue() {
        for (int capacity = 0; capacity < 100; ++capacity) {
            BoundedIntQueue q = newBoundedIntQueue(capacity);
            assertTrue(q.isEmpty());
        }
    }

    @Test
    public void isFullIsInitiallyFalse() {
        for (int capacity = 1; capacity < 100; ++capacity) {
            BoundedIntQueue q = newBoundedIntQueue(capacity);
            assertFalse(q.isFull());
        }
    }
}